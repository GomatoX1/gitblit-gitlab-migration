const gitlab = require('../gitlab');
const colors = require('colors');
const _ = require('lodash');

class CreatUser {
  constructor() {}
  create(email, password, reset_password, username, name, confirm, extern_uid, provider = 'ldapmain', external = false) {
   return new Promise((resolve, reject) => {
     gitlab.users.create({
       email, password, username, name, confirm, extern_uid, provider, external
     }, (response) => {
       resolve(response);
     });
   });
  }
  createEmail(username) {
    let email = username.split('');
    email.splice(1, 0, '.');
    return email.join('') + '@ba.lt';
  }
  generateLdapExternUid(displayName) {
    return `cn=${displayName.toLowerCase()},ou=ssc_users,ou=baltic amadeus users,dc=baltic-amadeus,dc=lt`
  }
}

module.exports = CreatUser;
