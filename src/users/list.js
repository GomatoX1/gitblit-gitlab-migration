const gitlab = require('../gitlab');
const colors = require('colors');
const _ = require('lodash');

class UsersList {
  constructor() {}
  async display() {
    return new Promise( async (resolve, reject) => {
      gitlab.users.all((users) => {
        resolve(users);
      });
    });
  }
}

module.exports = UsersList;
