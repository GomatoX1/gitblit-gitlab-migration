const gitlab = require('../gitlab');
const parser = require('../parser');
const userCreate = require('../users/create');
const usersList = require('../users/list');
const projectsList = require('../projects/list');
const colors = require('colors');
const _ = require('lodash');
const utils = require('../utils');

class AssignUsers {
  constructor() {
    this.utils = new utils();
    this.assignAllUsers();
  }
  async assignAllUsers() {
    const namespace_id = 97; // r group id
    // const namespace_id = 9; // r group id - docker
    const Parser = new parser();

    console.log('Starting to parse users.conf file..'.yellow);

    const users = await Parser.getFileInformation();

    console.log('Getting users list..'.yellow);

    await this.utils.asyncForEach(users, async (user, index) => {
      console.log(colors.green(`Got users information ${user.username} :: ${index}`));
      await this.createDataByUser(user, index);
    });
  }

  async createDataByUser(user) {
    return new Promise( async (resolve, reject) => {
      const UserCreate = new userCreate();
      console.log(colors.green(`Creating data by user: ${user.username}`));

      if (_.isNil(user.displayName)) {
        console.log(colors.red(`No display name: ${user.username}`));
        reject(`No display name: ${user.username}`);
      } else {

        const userData = {
          email: UserCreate.createEmail(user.username),
          password: 'A1234567',
          reset_password: true,
          username: user.username,
          name: user.displayName,
          confirm: false,
          extern_uid: UserCreate.generateLdapExternUid(user.displayName)
        };

        console.log(colors.yellow('Searching is user already created..'));
        const createdUser = await this.findUserByEmail(userData.email);

        if (!_.isNil(createdUser)) {
          console.log(colors.green(`User already created: ${user.username}`));

          if (!_.isNil(user.repositories)) {
            await this.utils.asyncForEach(user.repositories, async (repository, index) => {
              await this.assignUsersToProject(repository, createdUser.id);
            });

            resolve();
          }
        } else {
          console.log(colors.yellow(`User not exist: ${user.username}`));

          const newUser = await UserCreate.create(
            userData.email,
            userData.password,
            userData,
            userData.username,
            userData.name,
            userData.confirm,
            userData.extern_uid
          );

          if (!_.isNil(user.repositories)) {
            await this.utils.asyncForEach(user.repositories, async (repository, index) => {
              await this.assignUsersToProject(repository, newUser.id);
            });

            resolve();
          }
        }
      }
    });
  }

  async findUserByEmail(email) {
    const UsersList = new usersList();

    return new Promise( async (resolve, reject) => {
      const users = await UsersList.display()
      resolve(_.find(users, { email }));
    });
  }

  async findProjectByName(name) {
    return new Promise( async (resolve, reject) => {
      const ProjectsList = new projectsList();
      const projects = await ProjectsList.fetchFromGitlab();
      resolve(_.find(projects, { name }));
    });
  }

  async assignUsersToProject(repository, userId) {
    return new Promise( async (resolve, reject) => {
      const projectName = this.getRepositoryName(repository);
      const namespace_id = 96; // r group
      // const namespace_id = 4; // r group - docker
      const createdProject = await this.findProjectByName(projectName);

      if (!_.isNil(createdProject)) {
        console.log(colors.green(`Project ${projectName} found`));
        console.log({
          id: createdProject.id,
          user_id: userId,
          access_level: this.getPermissions(repository)
        });

        // assign user
        await this.addUserToProject(createdProject.id, userId, this.getPermissions(repository));
        console.log(colors.green(`User added to project ${projectName}`));

        resolve();
      } else {
        console.log(colors.red(`No such project, skipping! ${projectName}`));
        resolve();
      }
    });
  }

  getRepositoryName(repository) {
    const splitted = repository.split(':');
    return _.replace(splitted[1], '.git', '');
  }

  accessLevels(gitblitLevel) {
    const gitlabLevels = {
      'R': 20, // Reporter access
      'RW+': 30, // Developer access,
      'RWD': 30 // Developer access
    };

    return gitlabLevels[gitblitLevel];
  }

  getPermissions(repository) {
    const splitted = repository.split(':');
    return splitted[0];
  }

  async addUserToProject(projectId, userId, accessLevel) {
    return new Promise( async (resolve, reject) => {
      gitlab.projects.addMember({
        id: projectId,
        user_id: userId,
        access_level: this.accessLevels(accessLevel)
      }, response => {
        resolve(response);
      });
    });
  }
}

module.exports = AssignUsers;
