const gitlab = require('gitlab')({
  url:   process.env.GITLAB_URL,
  token: process.env.GITLAB_TOKEN
});

module.exports = gitlab;
