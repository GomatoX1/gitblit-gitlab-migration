const path = require('path');
const colors = require('colors');
const fs = require('fs');

class GitblitParser {
  constructor() {}
  getFileInformation() {
    return new Promise((resolve, reject) => {
      const config = fs.readFileSync(path.join(__dirname, '../../', 'data/users.conf'), 'utf-8');
      resolve(JSON.parse(config));
    });
  }
}

module.exports = GitblitParser;
