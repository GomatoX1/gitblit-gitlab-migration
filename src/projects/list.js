const gitlab = require('../gitlab');
const path = require('path');
const fs = require('fs');

class ProjectsList {
  async fetchFromGitlab() {
    return new Promise( async (resolve, reject) => {
      gitlab.projects.allAdmin((projects) => {
        resolve(projects);
      });
    });
  }
  async fetchFromConfig() {
    return new Promise((resolve, reject) => {
      const config = fs.readFileSync(path.join(__dirname, '../../', 'data/projects.conf'), 'utf-8');
      resolve(JSON.parse(config));
    });
  }
}

module.exports = ProjectsList;
