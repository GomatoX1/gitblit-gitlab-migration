const util = require('util');
const path = require('path');
const exec = util.promisify(require('child_process').exec);
const colors = require('colors');
const logger = require('../logger/logger');
const projectsList = require('./list');
const utils = require('../utils');

class Mirror {
  constructor() {
    this.tmpPath = path.join(__dirname, '..', '..', 'tmp');
    this.utils = new utils();
    this.onRun();
  }

  async onRun() {
    await this.mirrorAllProjects();
  }

  async mirrorAllProjects() {
    const ProjectsList = new projectsList();
    const projects = await ProjectsList.fetchFromConfig();
    await this.utils.asyncForEach(projects, async (project, index) => {
      // sukuriam projekta
      await this.mirror(project);
    });
  }

  async clone(repo) {
    return new Promise( async (resolve, reject) => {
      try {
        const { stdout, stderr } = await exec(`git clone --mirror ${repo} ${this.tmpPath}`);
        resolve(stderr);
      } catch (e) {
        reject(e);
      }
    });
  }

  async push(oldRepo, repo) {
    return new Promise( async (resolve, reject) => {
      const { stdout, stderr } = await exec(`git push --mirror ${repo}`, {
        cwd: this.tmpPath
      });
      resolve(stderr);
    });
  }

  async isRemoteEmpty(repo) {
    return new Promise( async (resolve, reject) => {
      const { stdout, stderr } = await exec(`git ls-remote ${repo}`);
      resolve(stdout.length === 0);
    });
  }

  async clearTmp() {
    return new Promise( async (resolve, reject) => {
      const { stdout, stderr } = await exec(`rm -rf ${this.tmpPath}`);
      resolve(stderr);
    });
  }

  async mirror(projectName) {
    console.log(colors.yellow.underline('Mirror in progress..'));

    return new Promise( async (resolve, reject) => {
      const oldRepo = `${process.env.GITBLIT_REPO}${projectName}.git`;
      const newRepo = `${process.env.GITLAB_REPO}${projectName}.git`;

      const isEmpty = await this.isRemoteEmpty(newRepo);

      if (isEmpty) {
        this.clone(oldRepo)
          .then( async () => {
            const gitLog = await this.push(oldRepo, newRepo);
            console.log(gitLog);
            await this.clearTmp();

            resolve({
              success: true
            });
          }).catch( async (error) => {
            const log = new logger();
            log.error(error);

            await this.clearTmp();

            resolve({
              success: true
            });
          });
      } else {
        console.log(colors.yellow.underline('Already mirrored, skipping'));
        resolve({
          success: true
        });
      }
    });
  }
}

module.exports = Mirror;
