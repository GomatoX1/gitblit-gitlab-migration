const gitlab = require('../gitlab');
const parser = require('../parser');
const userCreate = require('../users/create');
const usersList = require('../users/list');
const projectsList = require('../projects/list');
const mirror = require('../projects/mirror');
const colors = require('colors');
const _ = require('lodash');
const utils = require('../utils');

class CreateProjects {
  constructor() {
    // this.importProjects();
    this.utils = new utils();
    this.createAllProjects();
  }

  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }

  async createAllProjects() {
    // const ProjectsList = new projectsList();

    // const namespace_id = 9; // r group
    const namespace_id = 97; // r group

    const ProjectsList = new projectsList();
    const projects = await ProjectsList.fetchFromConfig();

    await this.utils.asyncForEach(projects, async (project, index) => {
      // sukuriam projekta
      await this.create(project, null, namespace_id);
      console.log(colors.green(`Project ${project} successfully created`));
    });
  }

  async importProjects() {
    const namespace_id = 97; // r group id
    // const namespace_id = 4; // r group id - docker
    const Parser = new parser();

    console.log('Starting to parse users.conf file..'.yellow);

    const users = await Parser.getFileInformation();

    console.log('Getting users list..'.yellow);

    await this.asyncForEach(users, async (user, index) => {
      console.log(colors.green(`Got users information ${user.username} :: ${index}`));
      await this.createDataByUser(user, index);
    });
  }

  async createDataByUser(user) {
    return new Promise( async (resolve, reject) => {
      const UserCreate = new userCreate();
      console.log(colors.green(`Creating data by user: ${user.username}`));

      if (_.isNil(user.displayName)) {
        console.log(colors.red(`No display name: ${user.username}`));
        reject(`No display name: ${user.username}`);
      } else {

        const userData = {
          email: UserCreate.createEmail(user.username),
          password: 'A1234567',
          reset_password: true,
          username: user.username,
          name: user.displayName,
          confirm: false,
          extern_uid: UserCreate.generateLdapExternUid(user.displayName)
        };

        console.log(colors.yellow('Searching is user already created..'));
        const createdUser = await this.findUserByEmail(userData.email);

        if (!_.isNil(createdUser)) {
          console.log(colors.green(`User already created: ${user.username}`));

          if (!_.isNil(user.repositories)) {
            await this.asyncForEach(user.repositories, async (repository, index) => {
              await this.assignUsersToProject(repository, createdUser.id);
            });

            resolve();
          }
        } else {
          console.log(colors.yellow(`User not exist: ${user.username}`));

          const newUser = await UserCreate.create(
            userData.email,
            userData.password,
            userData,
            userData.username,
            userData.name,
            userData.confirm,
            userData.extern_uid
          );

          if (!_.isNil(user.repositories)) {
            await this.asyncForEach(user.repositories, async (repository, index) => {
              await this.assignUsersToProject(repository, newUser.id);
            });

            resolve();
          }
        }
      }
    });
  }

  async findUserByEmail(email) {
    const UsersList = new usersList();

    return new Promise( async (resolve, reject) => {
      const users = await UsersList.display()
      resolve(_.find(users, { email }));
    });
  }

  async findProjectByName(name) {
    return new Promise( async (resolve, reject) => {
      const ProjectsList = new projectsList();
      const projects = await ProjectsList.get();
      resolve(_.find(projects, { name }));
    });
  }

  async assignUsersToProject(repository, userId) {
    const Mirror = new mirror();

    return new Promise( async (resolve, reject) => {
      const projectName = this.getRepositoryName(repository);
      const namespace_id = 97; // r group
      // const namespace_id = 4; // r group - docker
      const createdProject = await this.findProjectByName(projectName);

      if (!_.isNil(createdProject)) {
        console.log(colors.green(`Project ${projectName} found`));
        console.log({
          id: createdProject.id,
          user_id: userId,
          access_level: this.getPermissions(repository)
        });

        // assign user
        await this.addUserToProject(createdProject.id, userId, this.getPermissions(repository));
        console.log(colors.green(`User added to project ${projectName}`));

        // await Mirror.mirror(createdProject.http_url_to_repo, projectName);

        resolve();
      }
      // } else {
      //   console.log(colors.red(`Project ${projectName} not found`));
      //
      //   // create project and assign user
      //   const newProject = await this.create(projectName, null, namespace_id);
      //
      //   console.log(colors.green(`Project ${projectName} successfully created`));
      //   console.log({
      //     id: newProject.id,
      //     user_id: userId,
      //     access_level: this.getPermissions(repository)
      //   });
      //
      //   await this.addUserToProject(newProject.id, userId, this.getPermissions(repository));
      //   console.log(colors.green(`User added to project ${projectName}`));
      //
      //   await Mirror.mirror(newProject.http_url_to_repo, projectName);
      //   console.log(colors.green(`Project mirrored from gitblit ${projectName}`));
      //
      //   resolve();
      // }
    });
  }

  async create(
    name, // required
    path,
    namespace_id,
    description,
    issues_enabled,
    merge_requests_enabled,
    builds_enabled,
    wiki_enabled,
    snippets_enabled,
    container_registry_enabled,
    shared_runners_enabled,
    isPublic,
    visibility_level,
    import_url,
    public_builds,
    only_allow_merge_if_build_succeeds,
    only_allow_merge_if_all_discussions_are_resolved,
    lfs_enabled,
    request_access_enabled
  ) {
    console.log(name, path, namespace_id);

    return new Promise( async (resolve, reject) => {
      gitlab.projects.create({
        name,
        path,
        namespace_id,
        description,
        issues_enabled,
        merge_requests_enabled,
        builds_enabled,
        wiki_enabled,
        snippets_enabled,
        container_registry_enabled,
        shared_runners_enabled,
        public: isPublic,
        visibility_level,
        import_url,
        public_builds,
        only_allow_merge_if_build_succeeds,
        only_allow_merge_if_all_discussions_are_resolved,
        lfs_enabled,
        request_access_enabled
      }, response => {
        resolve(response);
      });
    });
  }

  getRepositoryName(repository) {
    const splitted = repository.split(':');
    return _.replace(splitted[1], '.git', '');
  }

  accessLevels(gitblitLevel) {
    // 10 // Guest access
    // 20 // Reporter access
    // 30 // Developer access
    // 40 // Master access
    // 50 // Owner access # Only valid for groups
    const gitlabLevels = {
      'R': 20, // Reporter access
      'RW+': 30, // Developer access,
      'RWD': 30 // Developer access
    };

    return gitlabLevels[gitblitLevel];
  }

  getPermissions(repository) {
    const splitted = repository.split(':');
    return splitted[0];
  }
  async addUserToProject(projectId, userId, accessLevel) {
    return new Promise( async (resolve, reject) => {
      gitlab.projects.addMember({
        id: projectId,
        user_id: userId,
        access_level: this.accessLevels(accessLevel)
      }, response => {
        resolve(response);
      });
    });
  }
}

module.exports = CreateProjects;
