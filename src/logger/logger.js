const fs = require('fs');
const path = require('path');

class Logger {
  constructor() {
    this.errorPath = path.join(__dirname, '..', '..', 'logs', 'error.log');
    this.infoPath = path.join(__dirname, '..', '..', 'logs', 'info.log');
  }

  async error(message) {
    return new Promise( async (resolve, reject) => {
      fs.appendFile(this.errorPath, `${this.getTime()}: ${message} \n`, (err) => {
        if (err) {
          reject(err)
        }

        resolve();
      });
    });
  }

  getTime() {
    const date = new Date();
    return `[${date}]`;
  }

  async info(message) {
    return new Promise( async (resolve, reject) => {
      fs.appendFile(this.infoPath, `${this.getTime()}: ${message} \n`, (err) => {
        if (err) {
          reject(err)
        }

        resolve();
      });
    });
  }
}

module.exports = Logger;
