const gitlab = require('../gitlab');

class GroupsList {
  constructor() {
    this.display();
  }
  display() {
    gitlab.groups.all((groups) => {
      console.log(groups);
    });
  }
}

module.exports = GroupsList;
