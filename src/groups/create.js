const gitlab = require('../gitlab');

class CreateGroups {
  constructor() {}
  create(name, path, description, visibility, lfs_enabled, request_access_enabled, parent_id) {
    gitlab.groups.create({
      name, // required
      path, // required
      description,
      visibility,
      lfs_enabled,
      request_access_enabled,
      parent_id
    }, (results) => {
      console.log(results);
    });
  }
}

module.exports = CreateGroups;
