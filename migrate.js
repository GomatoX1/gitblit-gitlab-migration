const dotenv = require('dotenv').config();
const _ = require('lodash');
const program = require('commander');

program
  .version('0.1.0')
  .option('-a, --action [value]', 'Action for script')
  .parse(process.argv);

switch (program.action) {
  case 'projects-list':
    const projectsList = require('./src/projects/list');
    const ProjectsList = new projectsList();
  break;

  case 'projects-create':
    const projectsCreate = require('./src/projects/create');
    const ProjectsCreate = new projectsCreate();
  break;

  case 'groups-create':
    const groupsCreate = require('./src/groups/create');
    const GroupsCreate = new groupsCreate();
  break;

  case 'groups-list':
    const groupsList = require('./src/groups/list');
    const GroupsList = new groupsList();
  break;

  case 'users-list':
    const usersList = require('./src/users/list');
    const UsersList = new usersList();
  break;

  case 'users-create':
    const usersCreate = require('./src/users/create');
    const UsersCreate = new usersCreate();
  break;

  case 'users-assign':
    const usersAssign = require('./src/users/assign');
    const UsersAssign = new usersAssign();
  break;

  case 'parse-users':
    const parser = require('./src/parser');
    const Parser = new parser();
  break;

  case 'mirror-projects':
    const mirror = require('./src/projects/mirror');
    const Mirror = new mirror();
  break;

  case 'logger':
    const logger = require('./src/logger/logger');
    const Logger = new logger();
  break;
}
