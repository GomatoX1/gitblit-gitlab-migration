## Gitblit -> Gitlab migration scripts
Order to migrate all stuff:

1. Create projects `node migrate.js --action projects-create`
2. Assign all users `node migrate.js --action users-assign`
3. Mirrora all projects `node migrate.js --action mirror-projects`

### Other commands:
* `node migrate.js --action projects-list`
* `node migrate.js --action project-create`
* `node migrate.js --action groups-create`
* `node migrate.js --action groups-list`
* `node migrate.js --action mirror-projects`
